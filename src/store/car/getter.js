export default {
  goodsObj: state => {
    return state.goods
  },
  goodsById: state => (id) => {
    return state.goods.filter(item => item.id === id)
  }
}
