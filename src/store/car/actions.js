/* eslint-disable no-unused-vars */
import { ADD } from './mutations-type'
export default {
  increment ({commit}, val) {
    commit('ADD', val)
  }
}
