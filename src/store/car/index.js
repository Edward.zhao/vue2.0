import getter from './getter'
import mutations from './mutations'
import actions from './actions'
export default {
  state: {
    goods: [
      {
        id: '0',
        name: '商品1',
        price: 100.00,
        num: 0
      },
      {
        id: '1',
        name: '商品2',
        price: 200.00,
        num: 0
      },
      {
        id: '2',
        name: '商品3',
        price: 300.00,
        num: 0
      }
    ],
    totalNum: 0,
    totalPrice: 0
  },
  getter,
  mutations,
  actions
}
