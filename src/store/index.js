import Vue from 'vue'
import Vuex from 'vuex'
import car from './car/index'
Vue.use(Vuex)

// const ADD = 'ADD'
export default new Vuex.Store({
  modules: {
    car
  }
//   state: {
//     goods: [
//       {
//         id: '0',
//         name: '商品1',
//         price: 100.00,
//         num: 0
//       },
//       {
//         id: '1',
//         name: '商品2',
//         price: 200.00,
//         num: 0
//       },
//       {
//         id: '2',
//         name: '商品3',
//         price: 300.00,
//         num: 0
//       }
//     ],
//     totalNum: 0,
//     totalPrice: 0
//   },
//   getters: {
//     goodsObj: state => {
//       return state.goods
//     },
//     goodsById: state => (id) => {
//       return state.goods.filter(item => item.id === id)
//     }
//   },
//   mutations: {
//     // add (state, {index}) {
//     //   state.goods[index].num++
//     //   state.totalNum++
//     //   state.totalPrice += state.goods[index].price
//     // }
//     [ADD] (state, {index}) {
//       state.goods[index].num++
//       state.totalNum++
//       state.totalPrice += state.goods[index].price
//     }
//   },
//   actions: {
//     increment ({commit}, val) {
//       commit('ADD', val)
//     }
//   }
})
